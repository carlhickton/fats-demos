﻿using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using Dapper;
using Newtonsoft.Json;

namespace GenerateData
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["SecurityConnection"].ConnectionString;

            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();
                var students = conn
                    .Query<StudentListItem>(@"
SELECT TOP 200 
  StudentId, 
  StudentCode, 
  GivenName AS FirstName, 
  SurName AS LastName ,
  Email,
  Gender,
  StatusId AS [Status]
FROm Student
")
                    .ToList();

                var json = JsonConvert.SerializeObject(students, Formatting.Indented);

                File.WriteAllText(@"C:\temp\Students.json", json);

                conn.Close();
            }
        }
    }

    public class StudentListItem
    {
        public int StudentId { get; set; }
        public string StudentCode { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Gender { get; set; }
        public string Status { get; set; }
    }
}