﻿module Demo {

    export class StudentService {

        constructor(private $http: ng.IHttpService, private $q: ng.IQService) {

        }

        getStudent(studentId: number): ng.IPromise<any> {
            var defer = this.$q.defer();

            this.$http.get(`/api/student/${studentId}`)
                .then(resp => {
                    defer.resolve(resp.data);
                }).catch(e => {
                    defer.reject(e);
                });

            return defer.promise;
        }

    }

    angular.module("app").service("StudentService", StudentService);

}