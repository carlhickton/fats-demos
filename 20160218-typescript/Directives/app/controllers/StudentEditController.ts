﻿module Demo {

    interface IStudentEdiParams extends ng.route.IRouteParamsService {
        id: number;
    }

    class StudentEditController {
        static $inject = ["$log", "$routeParams", "StudentService", "toastr"];

        student: any;
        studentId: number;

        constructor(private $log: ng.ILogService, $routeParams: IStudentEdiParams, private studentService: StudentService, private toastr: ngtoaster.IToasterService) {
            $log.debug("Opened Student Edit Controller");

            $log.debug(`Student Id: ${$routeParams.id}`);

            this.student = null;
            this.studentId = $routeParams.id;
            this.studentService.getStudent(this.studentId)
                .then(stu => {
                    this.student = stu;
                    this.$log.debug(stu);
                }).catch(e => {
                    this.$log.error(e);
                    this.toastr.error("Error loading Student");
                });

        }

        save() {
            this.toastr.error("Sorry not yet working");
        }

    }

    angular.module("app").controller("StudentEditController", StudentEditController);
}