﻿module Demo {
"use strict";

    function listPageActionBar() : ng.IDirective {
        return {
            restrict: "E",
            templateUrl: "/app/views/partials/ListPageActionBar.html",
            transclude: true,
            scope: {
                
            },
            replace: false
            
        };
    }

    angular.module("app").directive("listPageActionBar", listPageActionBar);
}