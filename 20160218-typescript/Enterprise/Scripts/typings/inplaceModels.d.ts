﻿declare module Inplace.Query {

    interface StudentCriteria extends KendoPagedCriteria {
        studentCode? : string;
        firstName? : string;
        lastName? : string;
    }

    interface KendoPagedCriteria {
        page?: number;
        pageSize?: number;
        skip?: number;
        take?: number;
        sort?: Inplace.Query.KendoPagedOrderBy[];
    }
    interface KendoPagedOrderBy {
        field?: string;
        dir?: string;
    }
    interface KendoPagedResults<T> {
        data: T[];
        total: number;
    }
}