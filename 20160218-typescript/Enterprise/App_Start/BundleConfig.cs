﻿using System.Web.Optimization;

namespace Enterprise
{
    public static class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/bootstrap.css",
                "~/Content/kendo.common-bootstrap.css",
                "~/Content/kendo.bootstrap.css",
                "~/Content/angular-toastr.css",
                "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/scripts/core").Include(
                "~/Scripts/jquery.js",
                "~/Scripts/bootstrap.js",
                "~/Scripts/kendo.all.js",
                "~/Scripts/angular.js",
                //"~/Scripts/angular-route.js",
                "~/Scripts/angular-ui-router.js",
                "~/Scripts/angular-messages.js",
                "~/Scripts/angular-toastr.js",
                "~/Scripts/kendo.angular.js"
                ));

            bundles.Add(new ScriptBundle("~/scripts/app").Include(
                "~/app/app.js",
                "~/app/controllers/*.js",
                "~/app/directives/*.js",
                "~/app/services/*.js"
                ));
        }
    }
}