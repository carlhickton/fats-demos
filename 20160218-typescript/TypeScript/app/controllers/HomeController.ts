﻿module Demo {
    "use strict";

    class HomeController {
        static $inject = ["$log"];

        constructor(private $log: ng.ILogService){
            $log.debug("Opened Home Controller");
        }
    }

    angular.module("app").controller("HomeController", HomeController);
}