﻿/// <reference path="../../Scripts/typings/angularjs/angular.d.ts"/>

module Demo {
    "use strict";

    class ContactController {
        static $inject = ["$log"];

        constructor(private $log: ng.ILogService) {
            $log.debug("Opened Contact Controller");
        }

        
    }

    angular.module("app").controller("ContactController", ContactController);
}