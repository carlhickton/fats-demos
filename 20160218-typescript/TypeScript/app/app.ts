﻿(() => {

    angular.module("app", [
        "ngRoute",
        "ngMessages",
        "toastr"
    ]).config(config);

    config.$inject = ["$routeProvider"];

    function config($routeProvider: ng.route.IRouteProvider) {

        $routeProvider
            .when("/",
            {
                templateUrl: "/app/views/Home.html",
                controller: "HomeController",
                controllerAs: "vm"
            })
            .when("/about",
            {
                templateUrl: "/app/views/about.html",
                controller: "AboutController",
                controllerAs: "vm"
            })
            .when("/contact",
            {
                templateUrl: "/app/views/contact.html",
                controller: "ContactController",
                controllerAs: "vm"
            });
    }

})()