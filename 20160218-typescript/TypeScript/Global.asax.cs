﻿using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using Enterprise;

namespace TypeScript
{
    public class Global : HttpApplication
    {
        protected void Application_Start()
        {
            MvcHandler.DisableMvcResponseHeader = true;
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            ServicePointManager.DefaultConnectionLimit = 256;
        }
    }
}